var fs = null;

module.exports = function(fs){
    var jsonGW_Obj = new jsonGW_class(fs);
    return jsonGW_Obj;
}
class jsonGW_class {
      constructor(fs) {
        this.fs =fs;
      }
      getjsonFromfile(path,callback){
        this.fs.readFile(path, 'utf8', function (err, data) {
              if (err) throw err;
              var obj = JSON.parse(data);
              callback(obj);
        });
      }
}
