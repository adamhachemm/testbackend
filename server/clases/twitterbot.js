const Twit  = require("twit");
module.exports=function(config,app,mdbconn){
    var _tw = new Twit(config);
      app.get("/getTweets/:username/:count",(req,res)=>{
                    console.log(req.query);
                    var datemin=req.query.datemin;
                    var datemax=req.query.datemax;
                    var params =req.params;
                    if(params.count=="all")
                            params.count=0;
                    _tw.get('statuses/user_timeline', { screen_name: params.username,count:params.count },  function (err, data, response) {
                        var tweets =data;
                        var tweetsres=[];
                        for (let index = 0; index < tweets.length; index++) {
                            const tweet = tweets[index];
                            
                            var datetweet = new Date(tweet.created_at);
                            
                            if(datemin){
                                datemin= new Date(datemin);
                                if(datetweet>datemin){
                                    if(datemax){
                                        datemax=new Date(datemax);
                                        if(datetweet < datemax){
                                            tweetsres.push(tweet);
                                            console.log(tweet);
                                        };
                                    }else{
                                        res.send({status:"err",response:"debes especificar fecha minimo y fecha maximo"});
                                        return;
                                    }
                                }
                            }else{
                                res.send({status:"err",response:"debes especificar fecha minimo y fecha maximo"});
                                return;
                            }

                            if(index+1 == tweets.length){
                                res.send({status:"OK",response:tweetsres});
                            }
                        }
                        
                    })
      });

      app.get("/getSummary/:username/:count",(req,res)=>{
                        var params =req.params;
                        _tw.get('users/show', { screen_name: params.username},  function (err, data, response) {
                            
                            if(data.errors){
                                res.send({status:"err",response:data.errors});
                                return;
                            }
                                var profileData= {
                                    name : data.name,
                                    location : data.location,
                                    description : data.description,
                                    profile_image : data.profile_image_url
                                 }
                                 var userSummary = {
                                    statusCount : data.statuses_count,
                                    lists : data.listed_count,
                                    followers : data.followers_count,
                                    favorites : data.status.favourites_count,
                                    reTweets_count : data.status.retweet_count
                                    }
                                    var tweets=[];
                                    _tw.get('statuses/user_timeline', {screen_name: params.username,count:params.count},  function (err, data, response) {
                                        var tweetsres = data;
                                        tweetsres.forEach(element=>{
                                            var tweet = {
                                                    tweet_text : element.text,
                                                    username : element.user.name,
                                                    id : element.id,
                                                    date : new Date(element.created_at)
                                                    }

                                                    tweets.push(tweet);
                                            })
                                            res.send({status:"OK",response:{profileData:profileData,tweets:tweets,userSummary:userSummary}});
                                        })
                                     })
                                 
    
                        });

  

        

      app.post("/saveTweets",(req,res)=>{
            var tweets = req.body.data;
            console.log(req.body);
            mdbconn.ConnectDB(conn=>{
                const collection = conn.db.collection("tweets").insertMany(tweets,(result)=>{
                    //console.log(result);
                    res.send({status:"ok",response:"tweets añadidos con exito en base de datos. Count : "+tweets.length});
            });
            });
      });

      app.get("/searchTweets",(req,res)=>{
          //params =>{q,result_type={mixed,recent,popular},lang,count,geocode=>{latitude,longitude,radius}}
          var params = req.query;
        
        var filter = JSON.parse(params.data);
        _tw.get('search/tweets', filter,  function (err, data, response) {
            res.send(data);
        })

    });

    
}