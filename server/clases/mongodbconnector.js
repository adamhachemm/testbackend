

module.exports = function(config,MongoClient){

  return new MongoDbConnector(config,MongoClient);
};
class MongoDbConnector {
  constructor(config,MongoClient) {
        this.MongoClient = MongoClient;
        //console.log(config);
        this.config = config;
  }

  ConnectDB(callback){
    var datab = this.config.db;
        this.MongoClient.connect("mongodb://"+this.config.user+":"+this.config.pwd+"@"+this.config.host+":"+this.config.port+"/"+this.config.db, function(err, db) {
          if(!err) {

            var database = {db:db.db(datab),conn:db};
            database.conn = db;
            callback(database);
          }else {
            console.log(err);
            process.exit();
          }
        });
  }
}
