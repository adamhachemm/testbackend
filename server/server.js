const _express     = require("express");
const app          = _express();
const _mongoClient = require("mongodb").MongoClient;
const _bodyParser  = require("body-parser");
const _router      = require("./router/router.js");
const _MongoDbConnector = require("./clases/mongodbconnector.js");
const _socketIo    = require("socket.io");
const fs = require('fs');


function perimitirCrossDomain(req, res, next) {
        //en vez de * se puede definir SÓLO los orígenes que permitimos
        res.header('Access-Control-Allow-Origin', '*'); 
        //metodos http permitidos para CORS
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE'); 
        res.header('Access-Control-Allow-Headers', 'Content-Type');
        next();
      }
      app.use(perimitirCrossDomain);
app.use(_bodyParser.urlencoded({ extended: false }));
app.use(_bodyParser.json());

app.set('views','../explorer/dist/explorer');
app.use(_express.static('../explorer/dist/explorer'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
//classes =========================================================================================================

const jsonGW = require("./clases/jsonGW_class");
const twitter = require("./clases/twitterbot.js");
var mdbconn;

//=================================================================================================================

var jsonfileReader = jsonGW(fs);

var serverConfig,models;
//Admin:Gaman579!@"+serverConfig.database.host+":"+serverConfig.database.port+"/"+serverConfig.database.database
jsonfileReader.getjsonFromfile("./server-config.json", function(obj){
        serverConfig = obj;
        // Connect to the db
        
          mdbconn = _MongoDbConnector({
                        host:serverConfig.database.host,
                        port:serverConfig.database.port,
                        db  :serverConfig.database.database,
                        pwd :serverConfig.database.pwd,
                        user:serverConfig.database.user
          },_mongoClient);
          
        //server listening
        var server = app.listen(obj.server.port,obj.server.host, ()=>{
                console.log("listening on : "+obj.server.host+":"+obj.server.port+" ==========================================>");
                const io = _socketIo(server);
                twitter(serverConfig.socialnetworks.twitter,app,mdbconn,io,fs);
                //twitterSetings
               
                _router(app,mdbconn,io);
        });
});
