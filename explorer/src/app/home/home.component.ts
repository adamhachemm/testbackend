import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public username:string;
  public count:number;
  public data:any;
  public json:string="asdasd";
  public respose:any;
  public showData:boolean=false;
  public datemin:Date;
  public datemax:Date;
  public save:boolean;
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  getSumary(){
    this.save=false;
    this.http.get("http://localhost/getSummary/"+this.username+"/"+this.count).subscribe((res:any)=>{
        console.log(res);
        this.data= res.response;
        this.showData=true;
        this.json= JSON.stringify(res.response,undefined,3);
    })
  }
  getTweets(){
   if(this.datemin && this.datemax && this.username){
    this.showData=false;
    this.http.get("http://localhost/getTweets/"+this.username+"/"+this.count+"?datemin="+this.datemin+"&datemax="+this.datemax).subscribe((res:any)=>{
        console.log(res);
        this.data= res.response;
        this.json= JSON.stringify(res.response,undefined,3);
        this.respose = res.response;
        this.save=true;
  });
   }else{
     alert("complete el formulario");
   }
  }

  saveTweets(){
    this.http.post("http://localhost/saveTweets",{data:this.respose}).subscribe((res:any)=>{
      console.log(res);
      this.data= res;
      this.json= JSON.stringify(res.response,undefined,3);
      this.save=false;
  })
  }

}
